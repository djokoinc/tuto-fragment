package com.app.happy.tutoandroid;

import android.support.v4.app.Fragment;

/**
 * Created by stagiaire on 19/05/2017.
 */

public class CrimeListActivity extends SingleFragmentActivity    {
    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
